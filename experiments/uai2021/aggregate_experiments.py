import os
from copy import copy
from pathlib import Path
from omegaconf import DictConfig, OmegaConf
import hydra
from hydra.experimental import compose, initialize
import numpy as np
import pandas as pd
import bson

import pickle
from copy import copy
from tqdm import tqdm

from support.database.records_aggregator import MongoAggregator
from support.database.mongo import get_mongo_observer, get_table

# Importing experiment suite from the parent directory.
import sys
from pathlib import Path
sys.path.append(str(Path('.').absolute().parent))



def _read_mongodb_res(cfg, run_switch_me, remote = False, remote_port = 27015, use_dumped_records = False):
    ## Get records
    if use_dumped_records:
        def get_records_from_dump(table_name=None):
            with open(Path('Records/mongodump') / f'20210105121154_mongodump_{table_name}/causal-data-augmentation/{table_name}.bson', 'rb') as f:
                data = pd.DataFrame(bson.decode_all(f.read()))
            return data
        get_records = get_records_from_dump
    else:
        def get_records_from_mongo(table_name=None):
            all_records = MongoAggregator(
                get_table(cfg.recording.table_name if table_name is None else table_name, cfg.database.mongo_host,
                        cfg.database.mongo_port if not remote else remote_port, cfg.database.mongo_user,
                        cfg.database.mongo_pass, cfg.database.mongo_dbname),
                query={}).get_results_pd(index=None)
            return all_records
        get_records = get_records_from_mongo


    all_records = {}
    for data_name, run in run_switch_me.items():
        if not run:
            continue
        all_records.update({data_name: get_records(f'main_{data_name}')})
    
    return all_records


def _agg_res(exp_res_me_dill, starting_run_id, ending_run_id):       
    # Filter results
    exp_res_me_stat = exp_res_me_dill.copy()
    exp_res_me_dill = exp_res_me_dill[exp_res_me_dill['method']=='proposed']
    exp_res_me_dill = exp_res_me_dill[exp_res_me_dill['sacred_run_id']>= starting_run_id]
    exp_res_me_dill = exp_res_me_dill[exp_res_me_dill['sacred_run_id']<= ending_run_id]

    data_name = exp_res_me_dill['data'].unique()[0]
    path_to_res = "pickle/data/" + data_name + "/"

    # Read fulldata
    data_run_id = exp_res_me_dill['data_run_id'].iloc[0]
    if exp_res_me_dill['preprocessing'].iloc[0] is not None:
        preprocessing_cfg = exp_res_me_dill['preprocessing'].iloc[0]
    else:
        preprocessing_cfg = []
    preprocess_suffix = ''
    for preprocess in preprocessing_cfg:
        key, val = list(preprocess.items())[0]
        preprocess_suffix += f'_{key}_{val}'
    fulldata_name = f'fulldata_{data_name}{preprocess_suffix}'

    path_file_to_fulldata = Path(str(sys.path[0]) + '/' + path_to_res + fulldata_name + ".dill")
    if path_file_to_fulldata.exists():
        with path_file_to_fulldata.open('rb') as _f:
            fulldata = pickle.load(_f) # the full dataset of the experiment
    else:
        print("Unknown file : " + str(path_file_to_fulldata))


    # Read other files and compute some stats
    df_stats = pd.DataFrame(columns = [
        'train_size', 
        'nb_aug', 
        'avg_weight_aug', 
        'std_weight_aug', 
        'nb_filtered', 
        'min_weight', 
        'max_weight', 
        'tot_weight', 
        'avg_weight_train', 
        'std_weight_train', 
        'XGB_baseline_MSE',
        'XGB_proposed_MSE',
        'delta_MSE_%',
        'data_run_id',
        'preprocessing',
        'data_cache_name',
        'data_cache_path',
        'method_predictor',
        'method_augmenter_config_name',
        'method_augmenter_config',
        'augmenter_config_weight_threshold',
        'aug_coeff'
        ])
    for run_ind in tqdm(range(0,exp_res_me_dill.shape[0])) :
        # Build again the base file name
        data_run_id = exp_res_me_dill['data_run_id'].iloc[run_ind]
        if exp_res_me_dill['preprocessing'].iloc[run_ind] is not None:
            preprocessing_cfg = exp_res_me_dill['preprocessing'].iloc[run_ind]
        else:
            preprocessing_cfg = []
        preprocess_suffix = ''
        for preprocess in preprocessing_cfg:
            key, val = list(preprocess.items())[0]
            preprocess_suffix += f'_{key}_{val}'
        fulldata_name = f'fulldata_{data_name}{preprocess_suffix}'
        suffix = copy(preprocess_suffix)
        suffix += f'_{data_run_id}'
        if exp_res_me_dill['validation_run'].iloc[run_ind] :
            data_train_size = np.round(exp_res_me_dill['train_size'].iloc[run_ind] / (exp_res_me_dill['train_size'].iloc[run_ind]+exp_res_me_dill['test_size'].iloc[run_ind]+exp_res_me_dill['validation_size'].iloc[run_ind]), 3)
            suffix += f'_ts{data_train_size}'
            data_validation_size = np.round(exp_res_me_dill['validation_size'].iloc[run_ind] / (exp_res_me_dill['train_size'].iloc[run_ind]+exp_res_me_dill['test_size'].iloc[run_ind]+exp_res_me_dill['validation_size'].iloc[run_ind]), 3)
            suffix += f'_vs{data_validation_size}'
        else:
            data_train_size = np.round(exp_res_me_dill['train_size'].iloc[run_ind] / (exp_res_me_dill['train_size'].iloc[run_ind]+exp_res_me_dill['test_size'].iloc[run_ind]), 3)
            suffix += f'_ts{data_train_size}'
        data_recording_set = exp_res_me_dill['recording_set'].iloc[run_ind]
        suffix += f'_run{data_recording_set}'
        data_cache_name = f'{data_name}{suffix}'
        file_to_open = data_cache_name + "_"
        
        # Open pickles
        all_pickles_opened = True
        path_file_to_data = Path(str(sys.path[0]) + '/' + path_to_res + file_to_open + "data.dill")
        if path_file_to_data.exists():
            with path_file_to_data.open('rb') as _f:
                train_df, test_df = pickle.load(_f) # train and test set used for this specific run
        else:
            print("Unknown file : " + str(path_file_to_data))
            all_pickles_opened = False
            
        path_file_to_pruned = Path(str(sys.path[0]) + '/' + path_to_res + file_to_open + "pruned.dill")
        if path_file_to_pruned.exists():
            with path_file_to_pruned.open('rb') as _f:
                pruned_df = pickle.load(_f) # train and test set used for this specific run
        else:
            print("Unknown file : " + str(path_file_to_pruned))
            all_pickles_opened = False

        path_file_to_aug = Path(str(sys.path[0]) + '/' + path_to_res + file_to_open + "augmented.dill")
        if path_file_to_aug.exists():
            with path_file_to_aug.open('rb') as _f:
                aug_df = pickle.load(_f) # train and test set used for this specific run
        else:
            print("Unknown file : " + str(path_file_to_aug))
            all_pickles_opened = False

        # path_file_to_indices = Path(path_to_res + "indices/" + file_to_open + "indices.dill")
        # if path_file_to_indices.exists():
        #     with path_file_to_indices.open('rb') as _f:
        #         ind0, ind1 = pickle.load(_f) # indices of the train and test data used for this specific run --> to reduce memory usage
        # else:
        #     print("Unknown file : " + str(path_file_to_indices))
        
        # Open xgb
        # TODO : à ouvrir pour ajouter des stats
            
        # Compute stats
        if all_pickles_opened:
            new_row_dict = {}
            new_row_dict['train_size'] = [train_df.shape[0]]
            new_row_dict['nb_aug'] = [aug_df.shape[0]]
            new_row_dict['avg_weight_aug'] = [aug_df['aug_weights'].mean()]
            new_row_dict['std_weight_aug'] = [aug_df['aug_weights'].std()]
            aug_df_merge = aug_df.copy()
            aug_df_merge['aug_sample'] = 1
            train_df_join_aug = train_df.merge(aug_df_merge, how="left", on=list(train_df.columns))
            new_row_dict['nb_filtered'] = [train_df_join_aug[train_df_join_aug['aug_sample'].isna()].shape[0]]
            new_row_dict['min_weight'] = [aug_df['aug_weights'].min()]
            new_row_dict['max_weight'] = [aug_df['aug_weights'].max()]
            new_row_dict['tot_weight'] = [aug_df['aug_weights'].sum() + pruned_df['aug_weights'].sum()]
            new_row_dict['avg_weight_train'] = [train_df_join_aug.dropna().aug_weights.mean()]
            new_row_dict['std_weight_train'] = [train_df_join_aug.dropna().aug_weights.std()]
            new_row_dict['XGB_baseline_MSE'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind] - 1]['XGB_MSE'].iloc[0]]
            new_row_dict['XGB_proposed_MSE'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['XGB_MSE'].iloc[0]]
            new_row_dict['delta_MSE_%'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['XGB_MSE'].iloc[0] - exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind] - 1]['XGB_MSE'].iloc[0]]
            new_row_dict['data_run_id'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['data_run_id'].iloc[0]]
            new_row_dict['preprocessing'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['preprocessing'].iloc[0]]
            new_row_dict['data_cache_name'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['data_cache_name'].iloc[0]]
            new_row_dict['data_cache_path'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['data_cache_path'].iloc[0]]
            new_row_dict['method_predictor'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['method_predictor'].iloc[0]]
            new_row_dict['method_augmenter_config_name'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['method_augmenter_config_name'].iloc[0]]
            new_row_dict['method_augmenter_config'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['method_augmenter_config'].iloc[0]]
            new_row_dict['augmenter_config_weight_threshold'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['augmenter_config_weight_threshold'].iloc[0]]
            new_row_dict['aug_coeff'] = [exp_res_me_stat[exp_res_me_stat['sacred_run_id'] == exp_res_me_dill['sacred_run_id'].iloc[run_ind]]['aug_coeff'].iloc[0]]
            df_stats = pd.concat([df_stats,pd.DataFrame.from_dict(new_row_dict)], ignore_index=True)

    # Save new stats
    path_to_save_csv = 'pickle/data/'
    path_to_save_csv += data_name
    path_to_save_csv += '/'
    path_to_save_csv += data_name
    path_to_save_csv += '_stats_runs_'
    path_to_save_csv += str(starting_run_id)
    path_to_save_csv += '_to_'
    path_to_save_csv += str(ending_run_id)
    path_to_save_csv += '.csv'
    df_stats.to_csv(str(sys.path[0]) + '/' + path_to_save_csv, index=False)


@hydra.main(config_path='config/config.yaml')
def main(cfg: DictConfig):
    
    # Read Mongodb results
    run_switch_me = dict(
        sachs = True,
        gss = True,
        auto_mpg = True,
        red_wine = True,
        white_wine = True,
        boston_housing = True,
    )
    all_records = _read_mongodb_res(cfg, run_switch_me)
    
    exp_res_me_dill = all_records[cfg.agg_res.experiment]
    ending_run_id = exp_res_me_dill['sacred_run_id'].iloc[exp_res_me_dill.shape[0]-1]
    starting_run_id = ending_run_id - cfg.agg_res.nb_repeats*cfg.agg_res.nb_train_sizes*2
    
    _agg_res(exp_res_me_dill, starting_run_id, ending_run_id)

    


if __name__ == '__main__':
    main()
