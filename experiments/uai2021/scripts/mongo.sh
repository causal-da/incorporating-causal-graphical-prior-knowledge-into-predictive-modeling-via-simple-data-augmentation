# source ./config.sh
mkdir -p /mnt/c/git/Causal Data Augmentation/ADMG/experiments/remote/mongodb
echo Starting mongodb at /mnt/c/git/Causal Data Augmentation/ADMG/experiments/remote/mongodb
tmux kill-session -t mongodb &
tmux new-session -d -s mongodb
tmux send-keys "mongod --dbpath=/mnt/c/git/Causal Data Augmentation/ADMG/experiments/remote/mongodb" ENTER
