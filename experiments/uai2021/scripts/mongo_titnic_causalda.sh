# source ./config.sh
mkdir ../../remote/mongodb
echo Starting mongodb at /home/causalda/experiments/remote/mongodb
tmux kill-session -t mongodb &
tmux new-session -d -s mongodb
tmux send-keys "mongod --dbpath=/home/causalda/experiments/remote/mongodb" ENTER
