#!/usr/bin/env bash
dataset=red_wine

val_prop=0

repeats_from=1
repeats_to=4

for train_size in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8; do
    for id in `seq $repeats_from $repeats_to`; do
        d="$dataset"_$id
        
        method=base_xgb
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size data.validation_size=$val_prop recording.table_name=main_$dataset data=$dataset debug=True

        method=proposed
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size data.validation_size=$val_prop recording.table_name=main_$dataset data=$dataset debug=True
    done
done