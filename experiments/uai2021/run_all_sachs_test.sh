#!/usr/bin/env bash
dataset=sachs

repeats_from=1
repeats_to=2

for train_size in 0.01 0.015 0.02; do
    for id in `seq $repeats_from $repeats_to`; do
        d="$dataset"_$id
        method=base_xgb
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size recording.table_name=main_$dataset data=$dataset debug=True

        method=proposed
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size recording.table_name=main_$dataset data=$dataset debug=True
    done
done


repeats_from=3
repeats_to=4

for train_size in 0.01 0.015 0.02; do
    for id in `seq $repeats_from $repeats_to`; do
        d="$dataset"_$id
        method=base_xgb
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size recording.table_name=main_$dataset data=$dataset debug=True

        method=proposed
        python run_experiment.py -m parallelization.data_run_id=$d method=$method data.train_size=$train_size recording.table_name=main_$dataset data=$dataset debug=True
    done
done

nb_train_sizes=3
nb_repeats=4

python aggregate_experiments.py -m agg_res.experiment=$dataset agg_res.nb_repeats=$nb_repeats agg_res.nb_train_sizes=$nb_train_sizes