FROM --platform=linux/amd64 ubuntu:20.04
# FROM divkal/nvidia-pytorch:1.5

LABEL MAINTAINER="apoinsot"

# Set console to not use prompts
ARG DEBIAN_FRONTEND=noninteractive

# Set python conda environment
ARG CONDA_ENV_NAME=causalda

# Set the username to be created
ARG USERNAME=${CONDA_ENV_NAME}
# Set working directory
ARG WORKDIR=/home/${USERNAME}

# Install the required packages
RUN apt-get update -y && \
    apt-get install -y apt-utils build-essential \
    curl cython gfortran git

RUN apt-get install -y tmux

RUN apt-get install -y libopenblas-dev libatlas-base-dev libblas-dev liblapack-dev
RUN apt-get install -y libmkl-full-dev #libmkl-dev python3-dev
RUN apt-get install -y python3-dev vim wget

# Create a new user for the image
#RUN useradd -rm -d /home/${USERNAME} -s /bin/bash -g ${USERNAME} -G sudo -u 1001 ${USERNAME}
RUN groupadd -r ${USERNAME} && \
    useradd --no-log-init -rm -d /home/${USERNAME} -s /bin/bash -g ${USERNAME} -G sudo -u 1001 ${USERNAME}

USER ${USERNAME}

ENV CONDA_HOME=/home/${USERNAME}/miniconda3
ENV PATH=$CONDA_HOME/bin:$PATH
ADD --chown=${USERNAME}:${USERNAME} . ${WORKDIR}

WORKDIR ${WORKDIR}

# Miniconda
# https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN curl -O https://repo.anaconda.com/miniconda/Miniconda3-py39_22.11.1-1-Linux-x86_64.sh && \
    bash Miniconda3-py39_22.11.1-1-Linux-x86_64.sh -b && \
    rm -f Miniconda3-py39_22.11.1-1-Linux-x86_64.sh   && \
    cd ${WORKDIR} && \
    conda init

# Mongodb
RUN curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.4.7.tgz && \
    tar xvf mongodb-linux-x86_64-3.4.7.tgz && \
    rm -f mongodb-linux-x86_64-3.4.7.tgz && \
    mv mongodb-linux-x86_64-3.4.7 mongodb

# Create a conda environment with the required dependencies
RUN conda env create -f environment.yml
# Activate created conda environment
RUN echo "conda activate ${CONDA_ENV_NAME}" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]

RUN pip install scikit-learn==0.23.2 numpy==1.19.1

# Install the required dependencies and download the data sets
RUN make install & \
    cd experiments/uai2021; pip install -r requirements.txt && \
    cd experiments/uai2021 				     && make decompress-records && \
    cd ${WORKDIR}/experiments/suite/data/auto_mpg           && make && \
    cd ${WORKDIR}/experiments/suite/data/boston_housing     && make && \
    cd ${WORKDIR}/experiments/suite/data/wine_quality       && make

